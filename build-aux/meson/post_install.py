#!/usr/bin/env python3

import sys
from compileall import compile_dir
from os import environ

if 'DESTDIR' not in environ and len(sys.argv) > 1:
    print('Compiling Python sources...')
    compile_dir(sys.argv[1], optimize=1)

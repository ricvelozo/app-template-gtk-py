from gi.repository import Gio, Gtk

from . import config


class Settings(Gio.Settings):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.do_changed('dark-mode')

    def do_changed(self, key):
        if key == 'dark-mode':
            settings = Gtk.Settings.get_default()
            settings.set_property('gtk-application-prefer-dark-theme',
                                  self.get_boolean('dark-mode'))

    def get_actions(self):
        actions = Gio.SimpleActionGroup.new()
        for key in self.props.settings_schema.list_keys():
            actions.add_action(self.create_action(key))

        return actions

from gi.repository import Gio, GLib, Gtk
from gi.repository.Gtk import MessageDialog

from . import config


@Gtk.Template(resource_path=config.PREFIX + 'main_window.ui')
class MainWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'MainWindow'

    left_headerbar = Gtk.Template.Child()
    right_headerbar = Gtk.Template.Child()
    menu_button = Gtk.Template.Child()
    recent_list_button = Gtk.Template.Child()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        application = kwargs['application']

        if config.CHANNEL != 'stable':
            self.get_style_context().add_class("devel")

        self.set_title(GLib.get_application_name() + ' - arquivo_1.xml')
        self.right_headerbar.set_subtitle('arquivo_1.xml')

        win_actions = Gio.SimpleActionGroup.new()
        win_actions.add_action_entries([
            ('new-project', self.unimplemented),
            ('open', self.unimplemented),
            ('save', self.unimplemented),
            ('save-as', self.unimplemented),
            ('show-inspector', self.on_inspector_clicked),
        ])
        self.insert_action_group('win', win_actions)
        self.insert_action_group(
            'settings', application.settings.get_actions())

    def unimplemented(self, action, parameter, user_data):
        from gi.repository.Gtk import ButtonsType, MessageType, ResponseType

        dialog = MessageDialog(self, None, MessageType.INFO, ButtonsType.OK,
                               _("This functionality will soon be available."),
                               title=_("Under development"))
        response = dialog.run()
        if response == ResponseType.OK:
            dialog.destroy()

    def on_inspector_clicked(self, action, parameter, user_data):
        self.set_interactive_debugging(True)

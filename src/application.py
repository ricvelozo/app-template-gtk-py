from gi.repository import Gdk, Gio, GLib, Gtk

from . import config
from .main_window import MainWindow
from .settings import Settings


class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args,
                         application_id='br.com.ricardoveloso.AppTemplate',
                         **kwargs)
        self.settings = Settings(self.props.application_id)

        # GTK3: Set a proper application ID
        GLib.set_prgname(self.props.application_id)
        GLib.set_application_name(_('App Template'))

        provider = Gtk.CssProvider()
        provider.load_from_resource(config.PREFIX + 'style.css')
        Ctx = Gtk.StyleContext
        Ctx.add_provider_for_screen(Gdk.Screen.get_default(), provider,
                                    Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    def do_startup(self):
        Gtk.Application.do_startup(self)

        set_accels = self.set_accels_for_action
        set_accels('app.help', ['F1'])
        set_accels('app.preferences', ['<Primary>comma'])
        set_accels('app.quit', ['<Primary>Q', '<Primary>W'])
        set_accels('app.show-help-overlay', ['<Primary>question'])
        set_accels('win.new-project', ['<Primary>N'])
        set_accels('win.open', ['<Primary>O'])
        set_accels('win.save', ['<Primary>S'])
        set_accels('win.save-as', ['<Primary><Shift>S'])

        actions = [
            ('quit', lambda *_: self.quit()),
        ]
        for name, handler in actions:
            action = Gio.SimpleAction.new(name)
            action.connect('activate', handler)
            self.add_action(action)

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = MainWindow(application=self)

        win.present()
